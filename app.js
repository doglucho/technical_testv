
function toggleButton() {
    const email = document.getElementById('email').validity.valid
    const residence = document.getElementById('residence').value
    const checkbox = document.getElementById('checkbox').checked

    if(email && residence && checkbox) {
        document.getElementById('submitButton').disabled = false
    } else {
        document.getElementById('submitButton').disabled = true
    }
}

document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('input[type=email]').forEach( node => node.addEventListener('keypress', e => {
      if(e.keyCode === 13) {
        e.preventDefault();
      }
    }))
})

function showMessage() {
    window.onload = function() {
        document.getElementById('submitButton').onclick = showMessage
    }
    return alert('¡Perfecto, te avisaremos la primera!')
}